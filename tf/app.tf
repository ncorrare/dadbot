variable "phrases" {
  type    = list(string)
  default = ["Either you are sitting at the table or part of the menu", "Lets see how it goes", "Have you seen the japanese candles", "I am very worried", "Your mum is nagging me", "You should cycle more", "When are doing a thriatlon together?"]
}
resource "random_shuffle" "phrases" {
  input        = var.phrases
  result_count = 1
}

output "phrase" {
    value = random_shuffle.phrases.result
    description = "A wise thought"
}



